[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# Usage

You need to have [installed poetry](https://python-poetry.org/docs/#installing-with-pipx). Call
```bash
poetry install
```
once, then you can run the script with
```bash
poetry run python flaschenscraper/main.py
```
from this directory.

# Tips
- In chrome dev console, right click request name and copy as curl. Then use https://curlconverter.com/ to get Python code.
- Copy the fixed json string in http://json.parser.online.fr/ to get a nice UI, which allows to collapse/extends part of the document.
import pprint

FLAPO_BASE_URL = "https://www.flaschenpost.de/"

import requests
import json
from config import COOKIES


def categories():
    import requests

    results = requests.get(
        "https://www.flaschenpost.de/elastic-query-portal/20/v2/categories",
        cookies=COOKIES,
    ).json()["results"]
    for category in results:
        for subcategory in category["subCategories"]:
            yield category["webShopUrl"], subcategory["webShopUrl"]


def get_product_json(category_url, subcategory_url):
    response = requests.get(
        f"{FLAPO_BASE_URL}{category_url}/{subcategory_url}",
        cookies=COOKIES,
    )

    line_prefix = (
        "            await window.CommandBus.send('product-service/cache-source', "
    )

    for line in response.text.splitlines():
        if line.startswith(line_prefix):
            # the "json" extracted from the site is not fully RFC compliant, we have to make some fixes
            json_string = (
                line[len(line_prefix) : -2]
                .replace("source:", '"source":', 1)
                .replace("products:", '"products":', 1)
                .replace("'", '"', 2)
            )
            return json.loads(json_string)


products = {}
for category_url, subcategory_url in categories():
    json_dict = get_product_json(category_url, subcategory_url)
    assert json_dict

    for product in json_dict["products"]:
        articles = {}
        for article in product.get("articles", []):
            articles[article["id"]] = article["shortDescription"]
        product_dict = {"name": product["name"], "articles": articles}
        pprint.pprint(product_dict)
        products[product["id"]] = product_dict
